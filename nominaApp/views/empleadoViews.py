from rest_framework import generics
from nominaApp.models import Empleado
from nominaApp.serializers import EmpleadoSerializer

class EmpleadoListViews(generics.ListCreateAPIView):
    queryset= Empleado.objects.all()
    serializer_class = EmpleadoSerializer
    
class EmpleadoDetailViews(generics.RetrieveUpdateDestroyAPIView):
    queryset= Empleado.objects.all()
    serializer_class = EmpleadoSerializer
    