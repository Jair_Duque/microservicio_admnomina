from rest_framework import generics
from nominaApp.models import Adelanto
from nominaApp.serializers import AdelantoSerializer

class AdelantoListViews(generics.ListCreateAPIView):
    queryset= Adelanto.objects.all()
    serializer_class = AdelantoSerializer
    
class AdelantoDetailViews(generics.RetrieveUpdateDestroyAPIView):
    queryset= Adelanto.objects.all()
    serializer_class = AdelantoSerializer
    
    
        
    