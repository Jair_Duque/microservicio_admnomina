from rest_framework import serializers
from nominaApp.models import Adelanto

class AdelantoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Adelanto
        fields = ['id','id_emp','Valor','fecha']