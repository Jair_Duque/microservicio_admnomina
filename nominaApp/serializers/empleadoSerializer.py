from rest_framework import serializers
from nominaApp.models import Empleado

class EmpleadoSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Empleado
        fields = ['id','nombre1','nombre2','apellido1','apellido2','documento','salario','fechaini_contrato']