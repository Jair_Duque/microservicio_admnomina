from django.db import models
from django.db.models.base import Model

class Adelanto(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_emp = models.BigIntegerField()
    Valor = models.BigIntegerField()
    fecha = models.DateField(default="0000-00-00")