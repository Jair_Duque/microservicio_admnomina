from django.db import models
from django.db.models.base import Model

class Empleado(models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre1 = models.CharField(max_length=20)
    nombre2 = models.CharField(max_length=20)
    apellido1 = models.CharField(max_length=20)
    apellido2 = models.CharField(max_length=20)
    documento = models.BigIntegerField()
    salario = models.BigIntegerField()
    fechaini_contrato = models.DateField(default="0000-00-00")